do_package_qa[noexec] = "1"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
# src as workspace path
FILESPATH =+ "${WORKSPACE}/:"

SRC_URI = "file://external/gstd-1.x/ \
            file://gstd \
            file://gstd.service \
"

S = "${WORKDIR}/external/gstd-1.x"

SRC_URI_remove_qti-distro-perf = "\
           file://0001-Disable-logging-on-perf-builds.patch \
           "

do_install_append() {
        rm -rf ${D}${sysconfdir}/default/gstd
        install -d ${D}${sysconfdir}/default
        install -m 0644 ${WORKDIR}/gstd ${D}${sysconfdir}/default/gstd

        install -d ${D}${systemd_system_unitdir}
        rm -f ${D}${systemd_system_unitdir}/gstd.service
        install -m 0644 ${WORKDIR}/gstd.service ${D}${systemd_system_unitdir}/gstd.service
}

FILES_${PN} += " \
                ${sysconfdir} \
                ${systemd_system_unitdir} \
               "

