SUMMARY = "QSEA open source package groups"
LICENSE          = "BSD-3-Clause-Clear"

PR="r1"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PACKAGES = " \
    packagegroup-qsea-platform \
    packagegroup-qsea-containerization \
    "

RDEPENDS_packagegroup-qsea-platform = " \
    gstd \
    mosquitto \
    redis \
    "

RDEPENDS_packagegroup-qsea-containerization = " \
    aufs-util \
    ca-certificates \
    chrony \
    cgroup-lite \
    docker \
    "
