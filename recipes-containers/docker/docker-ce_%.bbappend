FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "\
    file://daemon.json \
    file://CA-Certificate.crt \
    "

do_install_append(){
    install -m 644 ${WORKDIR}/daemon.json ${D}${sysconfdir}/docker/daemon.json
    install -d ${D}${sysconfdir}/docker/certs.d/global-registry-qsea-uat.zytersmartspaces.com
    install -m 0644 ${WORKDIR}/CA-Certificate.crt ${D}${sysconfdir}/docker/certs.d/global-registry-qsea-uat.zytersmartspaces.com/ca.crt
}
SYSTEMD_AUTO_ENABLE_${PN} = "enable"
SYSTEMD_SERVICE_${PN} += "${@bb.utils.contains('DISTRO_FEATURES','systemd','docker.service','',d)}"

FILES_${PN} += " \
                ${sysconfdir} \
                ${systemd_system_unitdir} \
                "
